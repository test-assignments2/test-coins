using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using UnityEngine;

namespace Coins
{
    public class Coin : MonoBehaviour
    {
        public static event Action AddCoin;

        [SerializeField] private AudioSource _audioSource;
        [SerializeField] private GameObject _model;
        [SerializeField] private Collider _collider;
        private CancellationTokenSource _cancellationTokenSource;

        private void Start()
        {
            _cancellationTokenSource = new CancellationTokenSource();
        }

        private void OnDisable()
        {
            _cancellationTokenSource.Cancel();
        }

        private void OnTriggerEnter(Collider other)
        {
            if (other.gameObject.CompareTag("Player"))
            {
                CollectCoin();
            }
        }

        private async Task CollectCoin()
        {
            AddCoin?.Invoke();
            _audioSource.Play();
            _model.SetActive(false);
            _collider.enabled = false;
            await Task.Delay(TimeSpan.FromSeconds(_audioSource.clip.length), _cancellationTokenSource.Token);
            Destroy(this.gameObject);
        }
    }
}


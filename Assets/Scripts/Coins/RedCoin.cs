using System;
using UnityEngine;

namespace Coins
{
    public class RedCoin : MonoBehaviour
    {
        public static event Action RestartGame;

        private void OnTriggerEnter(Collider other)
        {
            if (other.gameObject.CompareTag("Player"))
            {
                RestartGame?.Invoke();
            }
        }
    }
}
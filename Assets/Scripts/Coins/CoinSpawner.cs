using System;
using System.Collections.Generic;
using StarterAssets;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.InputSystem;
using UnityEngine.SceneManagement;
using Random = System.Random;

namespace Coins
{ 
    public class CoinSpawner : MonoBehaviour
   {
       [SerializeField] private Coin _coinPrefab;
       [SerializeField] private RedCoin _redCoinPrefab;
       [SerializeField] private int _coinCount = 10;
       [SerializeField] private int _redCoinCount = 3;
       [SerializeField] private List<Transform> _cointSpwawnPoints;
       

       private void Start()
       {
           SpawnCoins();
       }

       private void SpawnCoins()
       {
           Random random = new Random();
           int allCoins = _coinCount + _redCoinCount;
           for (int i = 0; i < allCoins; i++)
           {
               var index = random.Next(0, _cointSpwawnPoints.Count - 1);
               var position = _cointSpwawnPoints[index].position;
               _cointSpwawnPoints.RemoveAt(index);
               if (i < _coinCount)
               {
                   Instantiate(_coinPrefab, position, Quaternion.identity);
               }
               else
               {
                   Instantiate(_redCoinPrefab, position, Quaternion.identity);
               }
           }
       }
   } 
}

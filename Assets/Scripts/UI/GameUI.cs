using System;
using System.Threading;
using System.Threading.Tasks;
using Coins;
using TMPro;
using UnityEngine;
using UnityEngine.Serialization;

namespace UI
{
    public class GameUI : MonoBehaviour
    {
        [SerializeField] private GameObject _coinCounterPane;
        [SerializeField] private TextMeshProUGUI _text;
        [SerializeField] private GameObject _winPanel;
        
        public void SetText(string text)
        {
            _text.text = text;
        }

        public void ShowWin()
        {
            _coinCounterPane.SetActive(false);
            _winPanel.SetActive(true);
        }
    }
}
using System;
using System.Threading;
using Coins;
using UI;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.SceneManagement;


public class SceneLoader : MonoBehaviour
{
    [SerializeField] private GameUI _gameUI;
    [SerializeField] private int _amountCoinsForRestart = 10;
    [SerializeField] private int _timeToRestartAfterWin = 5;
    private InputSystem _input;
    private CancellationTokenSource _cancellationTokenSource;
    
    private void Awake()
    {
        _cancellationTokenSource = new CancellationTokenSource();
        CoinCounter counter = new CoinCounter(this, _gameUI, _cancellationTokenSource.Token,
            _amountCoinsForRestart, _timeToRestartAfterWin);
        _input = new InputSystem();
        _input.Game.Enable();
        _input.Game.Restart.started += (context => RestartGame());
        RedCoin.RestartGame += RestartGame;
    }

    private void OnDisable()
    {
        _cancellationTokenSource.Cancel();
        _input.Game.Restart.started -= (context => RestartGame());
        _input.Game.Disable();
        RedCoin.RestartGame -= RestartGame;
    }

    public void RestartGame()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }
}
using System;
using System.Threading;
using System.Threading.Tasks;
using Coins;
using UI;

public class CoinCounter
{
    private int _coins;
    private SceneLoader _sceneLoader;
    private GameUI _gameUI;
    private int _amountCoinsForRestart;
    private int _timeToRestartAfterWin;

    private CancellationToken _cancellationToken;

    public CoinCounter(SceneLoader sceneLoader, GameUI gameUI,
        CancellationToken cancellationToken, int amountCoinsForRestart,
        int timeToRestartAfterWin)
    {
        _sceneLoader = sceneLoader;
        _gameUI = gameUI;
        _amountCoinsForRestart = amountCoinsForRestart;
        _cancellationToken = cancellationToken;
        _timeToRestartAfterWin = timeToRestartAfterWin;
        
        
        Coin.AddCoin += AddCoin;
        
    }

    private void AddCoin()
    {
        _coins++;
        _gameUI.SetText($"Coins: {_coins}");
        if (_coins >= _amountCoinsForRestart)
        {
            Win();
        }
    }

    private async Task Win()
    {
        _gameUI.ShowWin();
        await Task.Delay(TimeSpan.FromSeconds(_timeToRestartAfterWin), _cancellationToken);
        _sceneLoader.RestartGame();
    }
    
}